package com.szoftverhurkak.loxon.javachallenge.secondturn.gamedata;

public class Command {
    private final int moveFrom;
    private final int moveTo;
    private final int armySize;

    public Command(int moveFrom, int moveTo, int armySize) {
        this.moveFrom = moveFrom;
        this.moveTo = moveTo;
        this.armySize = armySize;
    }

    public int getMoveFrom() {
        return moveFrom;
    }

    public int getMoveTo() {
        return moveTo;
    }

    public int getArmySize() {
        return armySize;
    }

    @Override
    public String toString() {
        return "{ \"moveFrom\": " + moveFrom + "," +
                "\"moveTo\": " + moveTo + "," +
                "\"armySize\": " + armySize +
                '}';
    }
}
