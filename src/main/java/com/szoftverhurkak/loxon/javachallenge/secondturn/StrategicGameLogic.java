package com.szoftverhurkak.loxon.javachallenge.secondturn;

import com.szoftverhurkak.loxon.javachallenge.secondturn.enchancedgamedata.PlanetWithState;
import com.szoftverhurkak.loxon.javachallenge.secondturn.gamedata.Command;
import com.szoftverhurkak.loxon.javachallenge.secondturn.gamedata.GameDescription;
import com.szoftverhurkak.loxon.javachallenge.secondturn.gamedata.GameState;
import com.szoftverhurkak.loxon.javachallenge.secondturn.gamedata.PlanetState;
import com.szoftverhurkak.loxon.javachallenge.secondturn.strategy.DummyStrategyDecider;
import com.szoftverhurkak.loxon.javachallenge.secondturn.strategy.Strategy;
import com.szoftverhurkak.loxon.javachallenge.secondturn.strategy.StrategyDecider;
import com.szoftverhurkak.loxon.javachallenge.secondturn.util.IntelligenceUtils;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class StrategicGameLogic implements GameLogic {

    private final List<GameState> gameStateHistory = new ArrayList<>();
    private final StrategyDecider strategyDecider = new DummyStrategyDecider();
    private final UniverseDrawer universeDrawer;
    private GameDescription gameDescription = null;

    private final Map<Integer, PlanetWithState> planets = new HashMap<>();

    public StrategicGameLogic(UniverseDrawer universeDrawer) {
        this.universeDrawer = universeDrawer;
    }

    @Override
    public void setGameDescription(GameDescription gameDescription, CommandSender commandSender) {
        this.gameDescription = gameDescription;
        IntelligenceUtils.setGameDescription(gameDescription);

        gameDescription.getPlanets().forEach(planet ->
                planets.put(planet.getPlanetID(), new PlanetWithState(planet, new PlanetState())));

        Strategy strategy = strategyDecider.decide(gameDescription, gameStateHistory, planets);
        Command command = strategy.getCommand(gameDescription, gameStateHistory, planets);
        System.out.println(command);
        commandSender.sendCommand(command);
    }

    @Override
    public void updateGameState(GameState gameState, CommandSender commandSender) {
        long start = System.currentTimeMillis();
        if (gameDescription == null) {
            gameDescription = GameDescription.createDummy();
            IntelligenceUtils.setGameDescription(gameDescription);
        }
        gameState.getPlanetStates().forEach(ps -> planets.get(ps.getPlanetID()).setPlanetState(ps));

        universeDrawer.drawPlanets(planets.entrySet().stream().map(Map.Entry::getValue).collect(Collectors.toList()));

        gameStateHistory.add(gameState);
        Strategy strategy = strategyDecider.decide(gameDescription, gameStateHistory, planets);
        Command command = strategy.getCommand(gameDescription, gameStateHistory, planets);
        if (command == null) {
            System.out.println("Doing nothing!");
        } else {
            System.out.println(command);
            commandSender.sendCommand(command);
        }
        System.out.println("Execution lasted: " + (System.currentTimeMillis() - start));
    }
}
