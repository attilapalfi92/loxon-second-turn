package com.szoftverhurkak.loxon.javachallenge.secondturn.strategy;

import com.szoftverhurkak.loxon.javachallenge.secondturn.enchancedgamedata.PlanetWithState;
import com.szoftverhurkak.loxon.javachallenge.secondturn.gamedata.*;
import com.szoftverhurkak.loxon.javachallenge.secondturn.util.IntelligenceUtils;
import com.szoftverhurkak.loxon.javachallenge.secondturn.util.Pair;
import com.szoftverhurkak.loxon.javachallenge.secondturn.util.SortedList;

import java.util.*;
import java.util.stream.Collectors;

/**
 * Balanced strategy
 */
public class AttilaStrategy implements Strategy {

    private final LinkedList<Command> commands = new LinkedList<>();
    private boolean firstCommands = true;
    private Map<Integer, PlanetWithState> planets = new HashMap<>();

    @Override
    public Command getCommand(GameDescription gameDescription, List<GameState> gameStateHistory, Map<Integer, PlanetWithState> planets) {
        this.planets = planets;
        final GameState gameState = gameStateHistory.get(gameStateHistory.size() - 1);
        if (!commands.isEmpty()) {
            return commands.removeFirst();
        }

        SortedList<Pair<Integer, StationedArmy>> armiesWithAttackingSize = IntelligenceUtils.getArmiesWithAttackingSize(planets.values());

        if (firstCommands) {
            Set<PlanetWithState> freePlanets = IntelligenceUtils.getFreePlanets(planets.values());
            executeFirstCommands(freePlanets, armiesWithAttackingSize);
            firstCommands = false;

        } else {
            Set<PlanetWithState> enemyPlanets = IntelligenceUtils.getEnemyPlanets(planets.values());
            Set<PlanetWithState> ourSafePlanets = IntelligenceUtils.getOurSafePlanets(planets.values());
            Set<PlanetWithState> planetsPredictedToBeInDanger = IntelligenceUtils.getPlanetsPredictedToBeInDanger(planets.values());
            SortedList<Pair<Integer, PlanetWithState>> freePlanetsWithArmySize = IntelligenceUtils.getFreePlanetsWithArmySize(planets.values());
            SortedList<Pair<Integer, PlanetWithState>> planetsInNeed = IntelligenceUtils.getPlanetsInNeed(planetsPredictedToBeInDanger);
            SortedList<Pair<Integer, PlanetWithState>> enemyPlanetsWithArmies = IntelligenceUtils.getEnemyPlanetsWithArmies(enemyPlanets);
            SortedList<Army> enemyArmies = IntelligenceUtils.getEnemyArmies(planets.values());

            SortedList<Pair<Integer, PlanetWithState>> allPlanetsToAttack
                    = freePlanetsWithArmySize.merge(planetsInNeed).merge(enemyPlanetsWithArmies);

            Optional<Command> command;
            // first we must evacuate attack armies who are in danger
            // TODO: or we could help them if possible. maybe helpOrEvacuateArmy solves it.

            splitLargestArmyInHalfIfNeededV2(enemyArmies, armiesWithAttackingSize);

            command = evacuateAttackingArmyInDanger(enemyPlanets, allPlanetsToAttack, ourSafePlanets);
            if (command.isPresent()) {
                commands.addLast(command.get());
                return commands.removeFirst();
            }

            if (weAreWinning(gameState)) {
                command = ratherAttackThanHelp(armiesWithAttackingSize, enemyPlanets, ourSafePlanets,
                        planetsInNeed, enemyArmies, allPlanetsToAttack);
                if (command.isPresent()) {
                    commands.addLast(command.get());
                    return commands.removeFirst();
                }

            } else {
                command = ratherHelpThanAttack(armiesWithAttackingSize, enemyPlanets, ourSafePlanets,
                        planetsInNeed, enemyArmies, allPlanetsToAttack);
                if (command.isPresent()) {
                    commands.addLast(command.get());
                    return commands.removeFirst();
                }
            }

            List<StatCalculation> singleActions
                    = IntelligenceUtils.getStatCalculationsForWinnableActions(armiesWithAttackingSize, allPlanetsToAttack);

            if (multiActionAttackIsPossible(singleActions, armiesWithAttackingSize, allPlanetsToAttack)) {
                return commands.removeFirst();

            } else {
                if (!singleActions.isEmpty()) {

                    if (weAreWinning(gameState)) {
                        command = forceOccupyPlanet(singleActions);
                        if (command.isPresent()) {
                            commands.addLast(command.get());
                            return commands.removeFirst();
                        }
                    }

                    commands.addLast(singleActions.get(0).getCommand().get());
                    return commands.removeFirst();
                }
            }
        }

        return commands.removeFirst();
    }

    // TODO: compare total time to single action's total time. also compare killed enemy unit numbers.

    private boolean multiActionAttackIsPossible(List<StatCalculation> singleActions,
                                                SortedList<Pair<Integer, StationedArmy>> armiesWithAttackingSize,
                                                SortedList<Pair<Integer, PlanetWithState>> allPlanetsToAttack) {
        List<StatCalculation> multiActions
                = IntelligenceUtils.getStatCalculationsForMergingArmies(armiesWithAttackingSize, allPlanetsToAttack);

        List<StatCalculation> filteredMultiActions = multiActions.stream()
                .filter(ma -> {
                    int sourcePlanetID1 = ma.getArmyWithSize().getRight().getPlanetID();
                    int sourcePlanetID2 = ma.getArmyWithSize2().getRight().getPlanetID();

                    Optional<StatCalculation> singleActionForSourcePlanet1 = singleActions.stream()
                            .filter(sa -> sa.getArmyWithSize().getRight().getPlanetID() == sourcePlanetID1)
                            .findFirst();


                    Optional<StatCalculation> singleActionForSourcePlanet2 = singleActions.stream()
                            .filter(sa -> sa.getArmyWithSize().getRight().getPlanetID() == sourcePlanetID2)
                            .findFirst();

                    // those multi actions will be kept where the two source armies cant do two things separately
                    return !singleActionForSourcePlanet1.isPresent() || !singleActionForSourcePlanet2.isPresent();
                })
                .sorted()
                .collect(Collectors.toList());

        Set<PlanetWithState> singleActionOccupiedNewPlanets = singleActions.stream()
                .filter(StatCalculation::isNewPlanetOccupied)
                .map(StatCalculation::getPlanet)
                .collect(Collectors.toSet());

        Set<PlanetWithState> multiActionOccupiedNewPlanets = multiActions.stream()
                .filter(StatCalculation::isNewPlanetOccupied)
                .map(StatCalculation::getPlanet)
                .collect(Collectors.toSet());

        multiActionOccupiedNewPlanets.removeAll(singleActionOccupiedNewPlanets);

        if (!multiActionOccupiedNewPlanets.isEmpty()) {
            Optional<StatCalculation> multiActionCalcWithNewPlanet = multiActions.stream()
                    .filter(ma -> multiActionOccupiedNewPlanets.contains(ma.getPlanet()))
                    .findFirst();

            if (multiActionCalcWithNewPlanet.isPresent()) {
                Command command1 = multiActionCalcWithNewPlanet.get().getCommand().get();
                Command command2 = multiActionCalcWithNewPlanet.get().getCommandForArmy2().get();
                System.out.println("------ DOING MULTI ACTION: " + command1 + " --- " + command2);
                commands.addLast(command1);
                commands.addLast(command2);
                return true;
            }
        }


        return false;
    }

    private Optional<Command> ratherHelpThanAttack(SortedList<Pair<Integer, StationedArmy>> armiesWithAttackingSize,
                                                   Set<PlanetWithState> enemyPlanets,
                                                   Set<PlanetWithState> ourSafePlanets,
                                                   SortedList<Pair<Integer, PlanetWithState>> planetsInNeed,
                                                   SortedList<Army> enemyArmies,
                                                   SortedList<Pair<Integer, PlanetWithState>> allPlanetsToAttack) {

        Optional<Command> command = helpOrEvacuateArmy(planetsInNeed, armiesWithAttackingSize, ourSafePlanets, allPlanetsToAttack);
        if (command.isPresent()) {
            return command;
        }
        command = largestArmyAttacksEnemyPlanet(armiesWithAttackingSize, enemyArmies, enemyPlanets);
        return command;

    }

    private Optional<Command> ratherAttackThanHelp(SortedList<Pair<Integer, StationedArmy>> armiesWithAttackingSize,
                                                   Set<PlanetWithState> enemyPlanets,
                                                   Set<PlanetWithState> ourSafePlanets,
                                                   SortedList<Pair<Integer, PlanetWithState>> planetsInNeed,
                                                   SortedList<Army> enemyArmies,
                                                   SortedList<Pair<Integer, PlanetWithState>> allPlanetsToAttack) {

        Optional<Command> command = largestArmyAttacksEnemyPlanet(armiesWithAttackingSize, enemyArmies, enemyPlanets);
        if (command.isPresent()) {
            return command;
        }
        command = helpOrEvacuateArmy(planetsInNeed, armiesWithAttackingSize, ourSafePlanets, allPlanetsToAttack);
        return command;
    }

    private Optional<Command> largestArmyAttacksEnemyPlanet(SortedList<Pair<Integer, StationedArmy>> armiesWithAttackingSize,
                                                            SortedList<Army> enemyArmies,
                                                            Set<PlanetWithState> enemyPlanets) {

        if (!armiesWithAttackingSize.isEmpty()) {
            Pair<Integer, StationedArmy> largestArmy = armiesWithAttackingSize.get(0);
            if (!enemyArmies.isEmpty()) {
                Army largestEnemyArmy = enemyArmies.get(0);
                if (largestArmy.getLeft() > largestEnemyArmy.getSize()) {
                    return enemyPlanets.stream()
                            .map(p -> new StatCalculation(largestArmy, p))
                            .filter(StatCalculation::isWinnable)
                            .sorted()
                            .findFirst()
                            .map(s -> s.getCommand().get());
                }
            }
        }
        return Optional.empty();
    }

    private Optional<Command> evacuateAttackingArmyInDanger(Set<PlanetWithState> enemyPlanets,
                                                            SortedList<Pair<Integer, PlanetWithState>> allPlanetsToAttack,
                                                            Set<PlanetWithState> ourSafePlanets) {

        List<StationedArmy> attackingArmiesInDanger = IntelligenceUtils.getAttackingArmiesInDanger(enemyPlanets);
        if (!attackingArmiesInDanger.isEmpty()) {
            StationedArmy ourAttackingArmy = attackingArmiesInDanger.get(0);
            return getCommandForEvacuation(ourAttackingArmy, allPlanetsToAttack, ourSafePlanets);
        }
        return Optional.empty();
    }

    private boolean weAreWinning(GameState gameState) {
        Standing us = gameState.getStandings().stream()
                .filter(s -> s.getUserID().equals(IntelligenceUtils.TEAM_NAME))
                .findFirst().get();

        return us.getStrength() > gameState.getStandings().stream()
                .filter(s -> !s.getUserID().equals(IntelligenceUtils.TEAM_NAME))
                .map(Standing::getStrength)
                .max(Integer::compare)
                .orElse(Integer.MAX_VALUE);
    }

    private Optional<Command> forceOccupyPlanet(List<StatCalculation> sortedCalculations) {
        return sortedCalculations.stream()
                .filter(StatCalculation::isNewPlanetOccupied)
                .sorted()
                .findFirst()
                .map(s -> s.getCommand().get());
    }

    private Optional<Command> helpOrEvacuateArmy(SortedList<Pair<Integer, PlanetWithState>> planetsInNeed,
                                                 SortedList<Pair<Integer, StationedArmy>> armiesWithAttackingSize,
                                                 Set<PlanetWithState> ourSafePlanets,
                                                 SortedList<Pair<Integer, PlanetWithState>> allPlanetsToAttack) {

        List<Pair<Integer, PlanetWithState>> planetsInNeedOrderedByOurArmySize = planetsInNeed.stream()
                .sorted((o1, o2) -> Integer.compare(IntelligenceUtils.getOurStationedArmySize(o2.getRight()),
                        IntelligenceUtils.getOurStationedArmySize(o1.getRight())))
                .collect(Collectors.toList());


        if (!planetsInNeedOrderedByOurArmySize.isEmpty()) {
            Pair<Integer, PlanetWithState> largestArmyInNeed = planetsInNeedOrderedByOurArmySize.get(0);
            Optional<StatCalculation> fastestHelp = armiesWithAttackingSize.stream()
                    .map(a -> new StatCalculation(a, largestArmyInNeed.getRight()))
                    .filter(StatCalculation::isWinnable)
                    .sorted()
                    .findFirst();

            return fastestHelp.map(StatCalculation::getCommand)
                    .orElseGet(() -> {
                        Optional<StationedArmy> stationedArmy = IntelligenceUtils.getOurStationedArmy(largestArmyInNeed.getRight());
                        if (stationedArmy.isPresent()) {
                            return getCommandForEvacuation(stationedArmy.get(), allPlanetsToAttack, ourSafePlanets);
                        } else {
                            return Optional.empty();
                        }
                    });
        }
        return Optional.empty();
    }


    // TODO: sometimes armies are not making the best decisions or not doing anything -> fix bug
    // TODO: somehow we could execute longer tactics with some armies
    // TODO: two armies could possibly take down some planets, that a single army cannot


    // TODO: when the army cannot go anywhere, just go to places so időhúzás
    private Optional<Command> getCommandForEvacuation(StationedArmy army,
                                                      SortedList<Pair<Integer, PlanetWithState>> allPlanetsToAttack,
                                                      Set<PlanetWithState> ourSafePlanets) {

        List<StatCalculation> attackCalculations = allPlanetsToAttack.stream()
                .filter(p -> army.getPlanetID() != p.getRight().getPlanet().getPlanetID())
                .map(p -> new StatCalculation(new Pair<>(army.getSize(), army), p.getRight()))
                .filter(StatCalculation::isWinnable)
                .filter(c -> c.getTotalTimeSpent() > 0)
                .sorted()
                .collect(Collectors.toList());

        if (!attackCalculations.isEmpty()) {
            return attackCalculations.get(0).getCommand();
        } else {

            return ourSafePlanets.stream()
                    .filter(p -> army.getPlanetID() != p.getPlanet().getPlanetID())
                    .map(p -> new StatCalculation(new Pair<>(army.getSize(), army), p))
                    .filter(StatCalculation::isWinnable)
                    .filter(c -> c.getTotalTimeSpent() > 0)
                    .sorted()
                    .findFirst()
                    .map(s -> s.getCommand().get());
        }
    }

    private void attackOrDefendPlanets(SortedList<Pair<Integer, PlanetWithState>> planetsToAttack,
                                       SortedList<Pair<Integer, PlanetWithState>> planetsInNeed,
                                       SortedList<Pair<Integer, StationedArmy>> armiesWithAttackingSize) {

        Optional<StatCalculation> attackCommand = attackClosestPlanet(planetsToAttack, armiesWithAttackingSize);
        Optional<StatCalculation> defendCommand = attackClosestPlanet(planetsInNeed, armiesWithAttackingSize);
    }

    private void executeFirstCommands(Set<PlanetWithState> freePlanets, SortedList<Pair<Integer, StationedArmy>> armiesWithAttackingSize) {
        Pair<Integer, StationedArmy> sizeOfArmy = armiesWithAttackingSize.get(0);
        StationedArmy firstArmy = sizeOfArmy.getRight();
        int firstAttackSize = sizeOfArmy.getLeft() / 2;
        sizeOfArmy.setLeft(sizeOfArmy.getLeft() - firstAttackSize);

        Optional<PlanetWithState> planet1 = IntelligenceUtils.getClosestPlanetToArmy(firstArmy, freePlanets);
        commands.addLast(new Command(firstArmy.getPlanetID(), planet1.get().getPlanet().getPlanetID(), firstAttackSize));

        freePlanets.remove(planet1.get());
        Optional<PlanetWithState> planet2 = IntelligenceUtils.getClosestPlanetToArmy(firstArmy, freePlanets);
        commands.addLast(new Command(firstArmy.getPlanetID(), planet2.get().getPlanet().getPlanetID(), sizeOfArmy.getLeft()));
    }

//    private void attackPlanets(SortedList<Pair<Integer, PlanetWithState>> planetsToAttackWithArmies,
//                               SortedList<Pair<Integer, StationedArmy>> armiesWithAttackingSize) {
//
//        SortedList<StatCalculation> statCalculations = new SortedList<>();
//        planetsToAttackWithArmies.stream()
//                .flatMap(planet -> armiesWithAttackingSize.stream().map(army -> new StatCalculation(army, planet.getRight())))
//                .forEach(statCalculations::addInOrder);
//
//        StatCalculation statCalculation = statCalculations.get(0);
//        if (statCalculation != null) {
//            commands.addLast(statCalculation.getCommand());
//        }
//
//        Optional<StatCalculation> decision = attackClosestPlanet(planetsToAttackWithArmies, armiesWithAttackingSize);
//        if (decision.isPresent()) {
//            commands.addLast(decision.get().getCommand());
//        } else {
//            attackPlanetsWithMultipleArmies(planetsToAttackWithArmies, armiesWithAttackingSize);
//        }
//    }

    private Optional<StatCalculation> attackClosestPlanet(SortedList<Pair<Integer, PlanetWithState>> planetsToAttackWithArmies,
                                                          SortedList<Pair<Integer, StationedArmy>> armiesWithAttackingSize) {

        if (!armiesWithAttackingSize.isEmpty()) {
//            splitLargestArmyInHalfIfNeeded(planetsToAttackWithArmies, armiesWithAttackingSize);
            Pair<Integer, StationedArmy> largestArmy = armiesWithAttackingSize.get(0);
            Optional<PlanetWithState> planetToAttack = IntelligenceUtils
                    .getClosestPlanetToArmyWithUnitConstraints(largestArmy, planetsToAttackWithArmies);

            return planetToAttack.map(planetWithState -> new StatCalculation(largestArmy, planetWithState));

        } else {
            return Optional.empty();
        }
    }

//    private void attackPlanetsWithMultipleArmies(SortedList<Pair<Integer, PlanetWithState>> planetsToAttackWithArmies, SortedList<Pair<Integer, StationedArmy>> armiesWithAttackingSize) {
//        for (Iterator<Pair<Integer, PlanetWithState>> enemyPlanetIter = planetsToAttackWithArmies.listIterator(); enemyPlanetIter.hasNext(); ) {
//            Pair<Integer, PlanetWithState> enemyPlanet = enemyPlanetIter.next();
//
//            // negative numbers in armies with attacking size --> investigate
//            int requiredSizeRemained = enemyPlanet.getLeft() + PLUS_UNITS_TO_WIN_BATTLE;
//            for (Iterator<Pair<Integer, StationedArmy>> armyWithSizeIter = armiesWithAttackingSize.listIterator();
//                 armyWithSizeIter.hasNext() && requiredSizeRemained != 0; ) {
//
//                Pair<Integer, StationedArmy> armyWithSize = armyWithSizeIter.next();
//
//                if (armyWithSize.getLeft() <= requiredSizeRemained) {
//                    armyWithSizeIter.remove();
//                    requiredSizeRemained -= armyWithSize.getLeft();
//                    commands.addLast(new Command(armyWithSize.getRight().getPlanetID(),
//                            enemyPlanet.getRight().getPlanet().getPlanetID(), armyWithSize.getLeft()));
//
//                } else {
//                    armyWithSizeIter.remove();
//                    requiredSizeRemained = 0;
//                    commands.addLast(new Command(armyWithSize.getRight().getPlanetID(),
//                            enemyPlanet.getRight().getPlanet().getPlanetID(), armyWithSize.getLeft()));
//                }
//            }
//        }
//    }

    private void splitLargestArmyInHalfIfNeeded(SortedList<Army> enemyArmies,
                                                SortedList<Pair<Integer, StationedArmy>> armiesWithAttackingSize) {

        Pair<Integer, StationedArmy> largestArmy = armiesWithAttackingSize.get(0);
        Integer largestSize = largestArmy.getLeft();
        StationedArmy stationedArmy = largestArmy.getRight();
        int sumOfTwoStrongestEnemyArmies = enemyArmies.stream().limit(2).mapToInt(Army::getSize).sum();
        if (sumOfTwoStrongestEnemyArmies < largestSize) {
            armiesWithAttackingSize.remove(0);
            armiesWithAttackingSize.addInOrder(new Pair<>(largestSize / 2, stationedArmy.copyWithNewSize(largestSize / 2)));
            armiesWithAttackingSize.addInOrder(new Pair<>(largestSize / 2, stationedArmy.copyWithNewSize(largestSize / 2)));
        }
    }

    private void splitLargestArmyInHalfIfNeededV2(SortedList<Army> enemyArmies,
                                                SortedList<Pair<Integer, StationedArmy>> armiesWithAttackingSize) {

        if (!armiesWithAttackingSize.isEmpty()) {
            Pair<Integer, StationedArmy> largestArmy = armiesWithAttackingSize.get(0);
            Integer largestSize = largestArmy.getLeft();
            if (largestSize > 100) {
                StationedArmy stationedArmy = largestArmy.getRight();
                armiesWithAttackingSize.remove(0);
                armiesWithAttackingSize.addInOrder(new Pair<>(largestSize / 2, stationedArmy.copyWithNewSize(largestSize / 2)));
                armiesWithAttackingSize.addInOrder(new Pair<>(largestSize / 2, stationedArmy.copyWithNewSize(largestSize / 2)));
            }
        }
    }
}
