package com.szoftverhurkak.loxon.javachallenge.secondturn;

import com.szoftverhurkak.loxon.javachallenge.secondturn.gamedata.GameDescription;
import com.szoftverhurkak.loxon.javachallenge.secondturn.gamedata.GameState;

public interface GameLogic {
    void setGameDescription(GameDescription gameDescription, CommandSender commandSender);

    void updateGameState(GameState gameState, CommandSender commandSender);
}
