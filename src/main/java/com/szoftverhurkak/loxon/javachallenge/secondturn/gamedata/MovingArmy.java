package com.szoftverhurkak.loxon.javachallenge.secondturn.gamedata;

public class MovingArmy extends Army {

    public MovingArmy() {
    }

    public MovingArmy(String owner, int size, int planetId, double x, double y) {
        super(owner, size, planetId, x, y);
    }

    @Override
    public MovingArmy copyWithNewSize(int size) {
        return new MovingArmy(owner, size, planetId, x, y);
    }
}
