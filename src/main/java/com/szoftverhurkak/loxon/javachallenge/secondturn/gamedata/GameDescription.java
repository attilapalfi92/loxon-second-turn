package com.szoftverhurkak.loxon.javachallenge.secondturn.gamedata;

import com.szoftverhurkak.loxon.javachallenge.secondturn.util.SortedList;

import java.util.ArrayList;
import java.util.List;

public class GameDescription {
    private int gameLength;
    private int mapSizeX;
    private int mapSizeY;
    private int commandSchedule;
    private int internalSchedule;
    private int broadcastSchedule;
    private int minMovableArmySize;
    private double movementSpeed;
    private double battleSpeed;
    private double captureSpeed;
    private double unitCreateSpeed;
    private double planetExponent;
    private double battleExponent;
    private SortedList<Planet> planets = new SortedList<>();
    private List<Player> players;

    public static GameDescription createDummy() {
        GameDescription gameDescription = new GameDescription();
        gameDescription.gameLength = 1;
        gameDescription.mapSizeX = 1;
        gameDescription.mapSizeY = 1;
        gameDescription.commandSchedule = 1;
        gameDescription.internalSchedule = 1;
        gameDescription.broadcastSchedule = 1;
        gameDescription.minMovableArmySize = 1;
        gameDescription.movementSpeed = 1;
        gameDescription.battleSpeed = 1;
        gameDescription.captureSpeed = 1;
        gameDescription.unitCreateSpeed = 1;
        gameDescription.planetExponent = 1;
        gameDescription.battleExponent = 1;
        gameDescription.planets = new SortedList<>();
        gameDescription.players = new ArrayList<>();
        return gameDescription;
    }

    public int getGameLength() {
        return gameLength;
    }

    public void setGameLength(int gameLength) {
        this.gameLength = gameLength;
    }

    public int getMapSizeX() {
        return mapSizeX;
    }

    public void setMapSizeX(int mapSizeX) {
        this.mapSizeX = mapSizeX;
    }

    public int getMapSizeY() {
        return mapSizeY;
    }

    public void setMapSizeY(int mapSizeY) {
        this.mapSizeY = mapSizeY;
    }

    public int getCommandSchedule() {
        return commandSchedule;
    }

    public void setCommandSchedule(int commandSchedule) {
        this.commandSchedule = commandSchedule;
    }

    public int getInternalSchedule() {
        return internalSchedule;
    }

    public void setInternalSchedule(int internalSchedule) {
        this.internalSchedule = internalSchedule;
    }

    public int getBroadcastSchedule() {
        return broadcastSchedule;
    }

    public void setBroadcastSchedule(int broadcastSchedule) {
        this.broadcastSchedule = broadcastSchedule;
    }

    public int getMinMovableArmySize() {
        return minMovableArmySize;
    }

    public void setMinMovableArmySize(int minMovableArmySize) {
        this.minMovableArmySize = minMovableArmySize;
    }

    public double getMovementSpeed() {
        return movementSpeed;
    }

    public void setMovementSpeed(double movementSpeed) {
        this.movementSpeed = movementSpeed;
    }

    public double getBattleSpeed() {
        return battleSpeed;
    }

    public void setBattleSpeed(double battleSpeed) {
        this.battleSpeed = battleSpeed;
    }

    public double getCaptureSpeed() {
        return captureSpeed;
    }

    public void setCaptureSpeed(double captureSpeed) {
        this.captureSpeed = captureSpeed;
    }

    public double getUnitCreateSpeed() {
        return unitCreateSpeed;
    }

    public void setUnitCreateSpeed(double unitCreateSpeed) {
        this.unitCreateSpeed = unitCreateSpeed;
    }

    public double getPlanetExponent() {
        return planetExponent;
    }

    public void setPlanetExponent(double planetExponent) {
        this.planetExponent = planetExponent;
    }

    public double getBattleExponent() {
        return battleExponent;
    }

    public void setBattleExponent(double battleExponent) {
        this.battleExponent = battleExponent;
    }

    public List<Planet> getPlanets() {
        return planets;
    }

    public void setPlanets(List<Planet> planets) {
        planets.forEach(this.planets::addInOrder);
    }

    public List<Player> getPlayers() {
        return players;
    }

    public void setPlayers(List<Player> players) {
        this.players = players;
    }
}
