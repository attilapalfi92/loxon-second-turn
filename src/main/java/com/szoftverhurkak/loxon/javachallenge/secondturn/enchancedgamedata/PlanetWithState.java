package com.szoftverhurkak.loxon.javachallenge.secondturn.enchancedgamedata;

import com.szoftverhurkak.loxon.javachallenge.secondturn.gamedata.Planet;
import com.szoftverhurkak.loxon.javachallenge.secondturn.gamedata.PlanetState;

public class PlanetWithState {
    private final Planet planet;
    private PlanetState planetState;

    public PlanetWithState(Planet planet, PlanetState planetState) {
        this.planet = planet;
        this.planetState = planetState;
        setArmyData(planet, planetState);
    }

    public Planet getPlanet() {
        return planet;
    }

    public PlanetState getPlanetState() {
        return planetState;
    }

    public void setPlanetState(PlanetState planetState) {
        this.planetState = planetState;
        setArmyData(planet, planetState);
    }

    private void setArmyData(Planet planet, PlanetState planetState) {
        planetState.getStationedArmies().forEach(sa -> {
            sa.setPlanetId(planet.getPlanetID());
            sa.setX(planet.getX());
            sa.setY(planet.getY());
        });
        planetState.getMovingArmies().forEach(ma -> {
            ma.setPlanetId(planet.getPlanetID());
        });
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        PlanetWithState that = (PlanetWithState) o;

        return planet != null ? planet.equals(that.planet) : that.planet == null;
    }

    @Override
    public int hashCode() {
        return planet != null ? planet.hashCode() : 0;
    }
}
