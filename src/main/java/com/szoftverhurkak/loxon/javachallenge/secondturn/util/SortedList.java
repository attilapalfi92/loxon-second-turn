package com.szoftverhurkak.loxon.javachallenge.secondturn.util;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;

public class SortedList<T extends Comparable<? super T>> extends ArrayList<T> {

    public void addInOrder(T element) {
        int insertionPoint = Collections.binarySearch(this, element);
        super.add((insertionPoint > -1) ? insertionPoint : (-insertionPoint) - 1, element);
    }

    public SortedList<T> merge(Collection<T> collection) {
        SortedList<T> results = new SortedList<>();
        results.addAll(this);
        collection.forEach(results::addInOrder);
        return results;
    }

    public T removeFirst() {
        return super.remove(0);
    }

    public T removeLast() {
        return super.remove(size() - 1);
    }

}
