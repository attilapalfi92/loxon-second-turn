package com.szoftverhurkak.loxon.javachallenge.secondturn.util;

public class Pair<L extends Comparable<L>, R> implements Comparable<Pair<L, R>> {

    private L left;
    private R right;

    public Pair(L left, R right) {
        this.left = left;
        this.right = right;
    }

    @Override
    public int compareTo(Pair<L, R> o) {
        return o.left.compareTo(left);
    }

    public L getLeft() {
        return left;
    }

    public void setLeft(L left) {
        this.left = left;
    }

    public R getRight() {
        return right;
    }

    public void setRight(R right) {
        this.right = right;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Pair<?, ?> pair = (Pair<?, ?>) o;

        return left != null ? left.equals(pair.left) : pair.left == null;
    }

    @Override
    public int hashCode() {
        return left != null ? left.hashCode() : 0;
    }
}
