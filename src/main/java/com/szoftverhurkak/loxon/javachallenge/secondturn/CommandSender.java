package com.szoftverhurkak.loxon.javachallenge.secondturn;

import com.szoftverhurkak.loxon.javachallenge.secondturn.gamedata.Command;

public interface CommandSender {
    void sendCommand(Command command);
}
