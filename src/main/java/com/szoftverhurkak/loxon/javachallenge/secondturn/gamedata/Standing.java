package com.szoftverhurkak.loxon.javachallenge.secondturn.gamedata;

public class Standing {
    private String userID;
    private int strength;
    private int score;

    public String getUserID() {
        return userID;
    }

    public void setUserID(String userID) {
        this.userID = userID;
    }

    public int getStrength() {
        return strength;
    }

    public void setStrength(int strength) {
        this.strength = strength;
    }

    public int getScore() {
        return score;
    }

    public void setScore(int score) {
        this.score = score;
    }
}
