package com.szoftverhurkak.loxon.javachallenge.secondturn.util;

import com.szoftverhurkak.loxon.javachallenge.secondturn.enchancedgamedata.PlanetWithState;
import com.szoftverhurkak.loxon.javachallenge.secondturn.gamedata.*;
import com.szoftverhurkak.loxon.javachallenge.secondturn.strategy.StatCalculation;

import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class IntelligenceUtils {

    public static final String TEAM_NAME = "szoftver";
    private static GameDescription gameDescription;

    public static List<Pair<Integer, PlanetWithState>> getPlanetsOrderedByDistanceToArmy(StationedArmy army,
                                                                                         SortedList<Pair<Integer, PlanetWithState>> enemyPlanetsWithArmies,
                                                                                         int limit) {
        return enemyPlanetsWithArmies.stream()
                .sorted((o1, o2) -> (int) (distance(army, o1.getRight()) - distance(army, o2.getRight())))
                .limit(limit)
                .collect(Collectors.toList());
    }

    /**
     * @param armyWithAttackingSize       army with an attacking size
     * @param planetsToAttackWithArmySize enemy planets with the number of armies needed to be owned
     * @return
     */
    public static Optional<PlanetWithState> getClosestPlanetToArmyWithUnitConstraints(Pair<Integer, StationedArmy> armyWithAttackingSize,
                                                                                      SortedList<Pair<Integer, PlanetWithState>> planetsToAttackWithArmySize) {
        List<Pair<Integer, PlanetWithState>> possibleEnemyPlanets = planetsToAttackWithArmySize.stream()
                .filter(enemyPlanetWithArmySize -> enemyPlanetWithArmySize.getLeft() < armyWithAttackingSize.getLeft())
                .collect(Collectors.toList());

        return getClosestPlanetToArmy(armyWithAttackingSize.getRight(),
                possibleEnemyPlanets.stream().map(Pair::getRight).collect(Collectors.toSet()));
    }

    /**
     * @param planetsPredictedToBeInDanger result of {@link IntelligenceUtils#getPlanetsPredictedToBeInDanger(Collection)}
     * @return planets that are under attack or moving armies approaching that are able to occupy it.
     * the integer value is the amount of armies needed to defend it
     */
    public static SortedList<Pair<Integer, PlanetWithState>> getPlanetsInNeed(Set<PlanetWithState> planetsPredictedToBeInDanger) {
        SortedList<Pair<Integer, PlanetWithState>> results = new SortedList<>();
        getNumberOfArmiesNeededToProtectOrAttackPlanet(planetsPredictedToBeInDanger)
                .forEach((planet, size) -> results.addInOrder(new Pair<>(size, planet)));
        return results;
    }

    /**
     * @param planetsPredictedToBeInDanger result of {@link IntelligenceUtils#getPlanetsPredictedToBeInDanger(Collection)}
     * @return planets that are under attack and the number of allied units standing on them
     */
    public static SortedList<Pair<Integer, PlanetWithState>> getEndangeredArmiesAndPlanets(Set<PlanetWithState> planetsPredictedToBeInDanger) {
        SortedList<Pair<Integer, PlanetWithState>> results = new SortedList<>();
        planetsPredictedToBeInDanger
                .forEach(planet -> results.addInOrder(new Pair<>(getOurStationedArmySize(planet), planet)));
        return results;
    }


    /**
     * @param armiesWithAttackingSize
     * @param allPlanetsToAttack
     * @return stat calcs in sorted order
     */
    public static List<StatCalculation> getStatCalculationsForWinnableActions(SortedList<Pair<Integer, StationedArmy>> armiesWithAttackingSize,
                                                                              SortedList<Pair<Integer, PlanetWithState>> allPlanetsToAttack) {
        return allPlanetsToAttack.stream()
                .flatMap(planet -> armiesWithAttackingSize.stream()
                        .filter(army -> army.getRight().getPlanetID() != planet.getRight().getPlanet().getPlanetID())
                        .map(army -> new StatCalculation(army, planet.getRight())))
                .filter(StatCalculation::isWinnable)
                .filter(c -> c.getTotalTimeSpent() > 0)
                .sorted()
                .collect(Collectors.toList());
    }


    /**
     * All the armies az merged with all the other armies, simulating every possible army merger for every possibly attackable planet.
     * only 2 armies are merged.
     * @param armiesWithAttackingSize armiesWithAttackingSize
     * @param allPlanetsToAttack allPlanetsToAttack
     * @return list of calcs for the mergers
     */
    public static List<StatCalculation> getStatCalculationsForMergingArmies(SortedList<Pair<Integer, StationedArmy>> armiesWithAttackingSize,
                                                                            SortedList<Pair<Integer, PlanetWithState>> allPlanetsToAttack) {

        return armiesWithAttackingSize.stream()
                .flatMap(army1 ->
                        allPlanetsToAttack.stream()
                                .filter(p -> army1.getRight().getPlanetID() != p.getRight().getPlanet().getPlanetID())
                                .flatMap(planet -> armiesWithAttackingSize.stream()
                                        .filter(army2 -> army2.getRight().getPlanetID() != planet.getRight().getPlanet().getPlanetID())
                                        .filter(army2 -> !army1.equals(army2))
                                        .map(army2 -> new StatCalculation(planet.getRight(), army1, army2)))
                                .filter(StatCalculation::isWinnable)
                                .filter(c -> c.getTotalTimeSpent() > 0)
                                .sorted()
                )
                .sorted()
                .collect(Collectors.toList());

    }

    public static int getOurStationedArmySize(PlanetWithState planet) {
        return planet.getPlanetState().getStationedArmies().stream()
                .filter(IntelligenceUtils::isOurArmy)
                .mapToInt(Army::getSize)
                .sum();
    }

    public static int getEnemyMovingArmySize(PlanetWithState planet) {
        return planet.getPlanetState().getMovingArmies().stream()
                .filter(a -> !isOurArmy(a))
                .mapToInt(Army::getSize)
                .sum();
    }

    public static int getAlliedMovingArmySize(PlanetWithState planet) {
        return planet.getPlanetState().getMovingArmies().stream()
                .filter(IntelligenceUtils::isOurArmy)
                .mapToInt(Army::getSize)
                .sum();
    }

    public static Set<PlanetWithState> getPlanetsWeHaveArmiesMovingTo(Collection<PlanetWithState> planets) {
        return planets.stream()
                .filter(p -> p.getPlanetState().getMovingArmies().stream().anyMatch(IntelligenceUtils::isOurArmy))
                .collect(Collectors.toSet());
    }

    public static Set<PlanetWithState> getAlliedPlanetsWeHaveArmiesMovingTo(Collection<PlanetWithState> planets) {
        return planets.stream()
                .filter(IntelligenceUtils::isOurPlanet)
                .filter(p -> p.getPlanetState().getMovingArmies().stream().anyMatch(IntelligenceUtils::isOurArmy))
                .collect(Collectors.toSet());
    }

    public static Set<PlanetWithState> getEnemyPlanetsWeHaveArmiesMovingTo(Collection<PlanetWithState> planets) {
        return planets.stream()
                .filter(p -> !isOurPlanet(p))
                .filter(p -> p.getPlanetState().getMovingArmies().stream().anyMatch(IntelligenceUtils::isOurArmy))
                .collect(Collectors.toSet());
    }

    /**
     * @param planets
     * @return the resulting stat calcs can be used to
     * determine when and how big armies will be available on which planet.
     */
    public static List<StatCalculation> getStatCalculationsForPlanets(Set<PlanetWithState> planets) {
        return planets.stream().map(StatCalculation::new).collect(Collectors.toList());
    }

    /**
     * @param enemyPlanets result of {@link IntelligenceUtils#getEnemyPlanets(Collection)}
     * @return ordered by size of army
     */
    public static List<StationedArmy> getAttackingArmiesInDanger(Set<PlanetWithState> enemyPlanets) {
        return enemyPlanets.stream()
                .filter(p -> p.getPlanetState().getStationedArmies().stream().anyMatch(IntelligenceUtils::isOurArmy))
                .filter(p -> getOurStationedArmySize(p) < getEnemyMovingArmySize(p))
                .flatMap(p -> p.getPlanetState().getStationedArmies().stream())
                .filter(IntelligenceUtils::isOurArmy)
                .sorted()
                .collect(Collectors.toList());
    }

    /**
     * @param planets a collection of planets
     * @return set of planets that we own but the summed number of enemy stationing and arriving armies are higher than ours
     */
    public static Set<PlanetWithState> getPlanetsPredictedToBeInDanger(Collection<PlanetWithState> planets) {
        return planets.stream()
                .filter(IntelligenceUtils::isOurPlanet)
                .filter(p -> {
                    int comingEnemySum = p.getPlanetState().getMovingArmies().stream()
                            .filter(ma -> !TEAM_NAME.equals(ma.getOwner()))
                            .mapToInt(Army::getSize)
                            .sum();

                    int arrivedEnemySum = p.getPlanetState().getStationedArmies().stream()
                            .filter(sa -> !TEAM_NAME.equals(sa.getOwner()))
                            .mapToInt(Army::getSize)
                            .sum();

                    int comingAlliedSum = p.getPlanetState().getMovingArmies().stream()
                            .filter(ma -> TEAM_NAME.equals(ma.getOwner()))
                            .mapToInt(Army::getSize)
                            .sum();

                    int arrivedAlliedSum = p.getPlanetState().getStationedArmies().stream()
                            .filter(sa -> TEAM_NAME.equals(sa.getOwner()))
                            .mapToInt(Army::getSize)
                            .sum();

                    return comingEnemySum + arrivedEnemySum > comingAlliedSum + arrivedAlliedSum;
                })
                .collect(Collectors.toSet());
    }

    public static SortedList<Pair<Integer, PlanetWithState>> getFreePlanetsWithArmySize(Collection<PlanetWithState> values) {
        Set<PlanetWithState> freePlanets = getFreePlanets(values);
        SortedList<Pair<Integer, PlanetWithState>> results = new SortedList<>();
        getNumberOfArmiesNeededToProtectOrAttackPlanet(freePlanets).entrySet().stream()
                .filter(e -> e.getValue() > 0)
                .forEach(e -> results.addInOrder(new Pair<>(e.getValue(), e.getKey())));
        return results;
    }

    /**
     * @param enemyPlanets result of {@link IntelligenceUtils#getEnemyPlanets(Collection)}
     * @return integer values are army sizes of enemy planets.
     */
    public static SortedList<Pair<Integer, PlanetWithState>> getEnemyPlanetsWithArmies(Set<PlanetWithState> enemyPlanets) {
        SortedList<Pair<Integer, PlanetWithState>> results = new SortedList<>();
        getNumberOfArmiesNeededToProtectOrAttackPlanet(enemyPlanets).entrySet().stream()
                .filter(e -> e.getValue() > 0)
                .forEach(e -> results.addInOrder(new Pair<>(e.getValue(), e.getKey())));
        return results;
    }

    /**
     * @param planets a set of planets
     * @return Set of planets which we own, but we don't have any army
     * on them because they are being occupied and in serious need for help
     */
    public static Set<PlanetWithState> getPlanetsAlmostTakenFromUs(Set<PlanetWithState> planets) {
        return planets.stream()
                .filter(IntelligenceUtils::isOurPlanet)
                .filter(p -> p.getPlanetState().getMovingArmies().stream().noneMatch(ma -> TEAM_NAME.equals(ma.getOwner())))
                .filter(p -> p.getPlanetState().getStationedArmies().stream().noneMatch(sm -> TEAM_NAME.equals(sm.getOwner())))
                .collect(Collectors.toSet());
    }

    /**
     * @param planets those set of planets whose are in need
     * @return bare minimum number of armies need to protect or attack a planet
     */
    public static Map<PlanetWithState, Integer> getNumberOfArmiesNeededToProtectOrAttackPlanet(Set<PlanetWithState> planets) {
        return planets.stream()
                .collect(Collectors.toMap(p -> p, p -> {
                    int comingEnemySum = p.getPlanetState().getMovingArmies().stream()
                            .filter(ma -> !TEAM_NAME.equals(ma.getOwner()))
                            .mapToInt(Army::getSize)
                            .sum();

                    int arrivedEnemySum = p.getPlanetState().getStationedArmies().stream()
                            .filter(sa -> !TEAM_NAME.equals(sa.getOwner()))
                            .mapToInt(Army::getSize)
                            .sum();

                    int comingAlliedSum = p.getPlanetState().getMovingArmies().stream()
                            .filter(ma -> TEAM_NAME.equals(ma.getOwner()))
                            .mapToInt(Army::getSize)
                            .sum();

                    int arrivedAlliedSum = p.getPlanetState().getStationedArmies().stream()
                            .filter(sa -> TEAM_NAME.equals(sa.getOwner()))
                            .mapToInt(Army::getSize)
                            .sum();

                    return comingEnemySum + arrivedEnemySum - comingAlliedSum - arrivedAlliedSum + 1;
                }));
    }

    /**
     * @param planets
     * @return our stationed armies and the number of movable units from those armies
     */
    public static SortedList<Pair<Integer, StationedArmy>> getArmiesWithAttackingSize(Collection<PlanetWithState> planets) {
        SortedList<Pair<Integer, StationedArmy>> results = new SortedList<>();
        Map<StationedArmy, List<MovingArmy>> stationedArmies = IntelligenceUtils.getStationedArmiesWithAttackinArmyList(planets);
        stationedArmies.entrySet().stream()
                .map((entry) -> {
                    StationedArmy army = entry.getKey();
                    List<MovingArmy> attackingEnemies = entry.getValue();
                    int armySize = army.getSize();
                    int attackingSize = attackingEnemies.stream().mapToInt(Army::getSize).sum();
                    int effectiveSize = armySize - attackingSize;
                    return new Pair<>(effectiveSize, army);
                })
                .filter(p -> p.getLeft() > 0)
                .forEach(results::addInOrder);
        return results;
    }

    public static Optional<PlanetWithState> getClosestPlanetToArmy(Army army, Collection<PlanetWithState> planets) {
        PlanetWithState closest = new PlanetWithState(
                new Planet(100000, Integer.MAX_VALUE, Integer.MAX_VALUE, 100000), new PlanetState());

        for (PlanetWithState p : planets) {
            if (distance(army, p) < distance(army, closest)) {
                closest = p;
            }
        }
        if (closest.getPlanet().getY() == Integer.MAX_VALUE) {
            return Optional.empty();
        }
        return Optional.of(closest);
    }

    public static Optional<PlanetWithState> getClosestSafePlanetToPlanet(PlanetWithState planet,
                                                                         Collection<PlanetWithState> ourSafePlanets) {
        PlanetWithState closest = new PlanetWithState(
                new Planet(100000, Integer.MAX_VALUE, Integer.MAX_VALUE, 100000), new PlanetState());

        for (PlanetWithState p : ourSafePlanets) {
            if (distance(planet, p) < distance(planet, closest)) {
                closest = p;
            }
        }
        if (closest.getPlanet().getY() == Integer.MAX_VALUE) {
            return Optional.empty();
        }
        return Optional.of(closest);
    }

    public static double distance(Army army, PlanetWithState planetWithState) {
        double xPower2 = Math.pow(army.getX() - planetWithState.getPlanet().getX(), 2);
        double yPower2 = Math.pow(army.getY() - planetWithState.getPlanet().getY(), 2);
        return Math.sqrt(xPower2 + yPower2);
    }

    public static double distance(PlanetWithState p1, PlanetWithState p2) {
        double xPower2 = Math.pow(p1.getPlanet().getX() - p2.getPlanet().getX(), 2);
        double yPower2 = Math.pow(p1.getPlanet().getY() - p2.getPlanet().getY(), 2);
        return Math.sqrt(xPower2 + yPower2);
    }

    /**
     * @param planets
     * @return our stationed armies and the list of enemy armies arriving to them
     */
    public static Map<StationedArmy, List<MovingArmy>> getStationedArmiesWithAttackinArmyList(Collection<PlanetWithState> planets) {
        Set<PlanetWithState> ourPeacefulPlanets = getOurPlanets(planets);
        Map<StationedArmy, List<MovingArmy>> results = new HashMap<>();
        ourPeacefulPlanets.forEach(planet -> {
                    final StationedArmy army;
                    List<StationedArmy> stationedArmies = planet.getPlanetState().getStationedArmies();
                    if (stationedArmies.isEmpty()) {
                        army = new StationedArmy(planet.getPlanetState().getOwner(), 0,
                                planet.getPlanet().getPlanetID(), planet.getPlanet().getX(), planet.getPlanet().getY());
                    } else {
                        army = stationedArmies.get(0);
                    }
                    results.put(army, planet.getPlanetState().getMovingArmies()
                            .stream()
                            .filter(ma -> !ma.getOwner().equals("szoftver"))
                            .collect(Collectors.toList()));
                }
        );
        return results;
    }

    public static Set<PlanetWithState> getFreePlanets(Collection<PlanetWithState> planets) {
        return planets
                .stream()
                .filter(p -> p.getPlanetState().getOwner() == null)
                .collect(Collectors.toSet());
    }

    /**
     * @param planets
     * @return retrieves those planets which are not in danger at all
     */
    public static Set<PlanetWithState> getOurSafePlanets(Collection<PlanetWithState> planets) {
        return planets
                .stream()
                .filter(IntelligenceUtils::isOurPlanet)
                .filter(p -> p.getPlanetState().getStationedArmies().stream().allMatch(sm -> TEAM_NAME.equals(sm.getOwner())))
                .filter(p -> p.getPlanetState().getMovingArmies().stream().allMatch(ma -> TEAM_NAME.equals(ma.getOwner())))
                .collect(Collectors.toSet());
    }

    public static boolean isOurPlanet(PlanetWithState p) {
        return TEAM_NAME.equals(p.getPlanetState().getOwner());
    }

    /**
     * @param planets
     * @return planets owned by us. probably armies are approaching them.
     */
    public static Set<PlanetWithState> getOurPlanets(Collection<PlanetWithState> planets) {
        return planets
                .stream()
                .filter(IntelligenceUtils::isOurPlanet)
                .filter(p -> p.getPlanetState().getOwnershipRatio() == 1)
                .filter(p -> p.getPlanetState().getStationedArmies().size() <= 1)
                .collect(Collectors.toSet());
    }

    public static Set<PlanetWithState> getNotOurPlanets(Collection<PlanetWithState> planets) {
        return planets
                .stream()
                .filter(p -> !isOurPlanet(p))
                .collect(Collectors.toSet());
    }

    public static Set<PlanetWithState> getEnemyPlanets(Collection<PlanetWithState> planets) {
        return planets
                .stream()
                .filter(p -> p.getPlanetState().getOwner() != null)
                .filter(p -> !isOurPlanet(p))
                .collect(Collectors.toSet());
    }

    public static Optional<StationedArmy> getStationaryEnemyArmy(PlanetWithState planet) {
        return planet.getPlanetState().getStationedArmies().stream()
                .filter(sa -> !isOurArmy(sa))
                .findFirst();
    }

    public static Optional<StationedArmy> getStationaryAlliedArmy(PlanetWithState planet) {
        return planet.getPlanetState().getStationedArmies().stream()
                .filter(IntelligenceUtils::isOurArmy)
                .findFirst();
    }

    /**
     * @param planet
     * @return ordered list of all arriving. ordering based on the time of arrival (seconds)
     */
    public static SortedList<Pair<Double, Army>> getAllArrivingArmies(PlanetWithState planet) {
        SortedList<Pair<Double, Army>> results = new SortedList<>();
        planet.getPlanetState().getMovingArmies().stream()
                .forEach(ma -> results.addInOrder(new Pair<>(distance(ma, planet) / gameDescription.getMovementSpeed(), ma)));
        return results;
    }

    /**
     * @param planet
     * @return ordered list of arriving enemies. ordering based on the time of arrival (seconds)
     */
    public static SortedList<Pair<Double, Army>> getArrivingEnemyArmies(PlanetWithState planet) {
        SortedList<Pair<Double, Army>> results = new SortedList<>();
        planet.getPlanetState().getMovingArmies().stream()
                .filter(ma -> !isOurArmy(ma))
                .forEach(ma -> results.addInOrder(new Pair<>(distance(ma, planet) / gameDescription.getMovementSpeed(), ma)));
        return results;
    }

    /**
     * @param planet
     * @return ordered list of arriving allies. ordering based on the time of arrival (seconds)
     */
    public static SortedList<Pair<Double, Army>> getArrivingAlliedArmies(PlanetWithState planet) {
        SortedList<Pair<Double, Army>> results = new SortedList<>();
        planet.getPlanetState().getMovingArmies().stream()
                .filter(IntelligenceUtils::isOurArmy)
                .forEach(ma -> results.addInOrder(new Pair<>(distance(ma, planet) / gameDescription.getMovementSpeed(), ma)));
        return results;
    }

    public static boolean isOurArmy(Army a) {
        return a.getOwner().equals(TEAM_NAME);
    }

    public static void setGameDescription(GameDescription gameDescription) {
        IntelligenceUtils.gameDescription = gameDescription;
    }

    public static GameDescription getGameDescription() {
        return gameDescription;
    }

    public static SortedList<Army> getEnemyArmies(Collection<PlanetWithState> values) {
        SortedList<Army> results = new SortedList<>();
        values.stream().map(PlanetWithState::getPlanetState).forEach(p -> {
            p.getStationedArmies().stream().filter(a -> !isOurArmy(a)).forEach(results::addInOrder);
            p.getMovingArmies().stream().filter(a -> !isOurArmy(a)).forEach(results::addInOrder);

        });
        return results;
    }

    public static Optional<StationedArmy> getOurStationedArmy(PlanetWithState planetWithState) {
        return planetWithState.getPlanetState().getStationedArmies().stream()
                .filter(IntelligenceUtils::isOurArmy)
                .sorted()
                .findFirst();
    }
}
