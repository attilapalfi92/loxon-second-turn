package com.szoftverhurkak.loxon.javachallenge.secondturn;

import com.szoftverhurkak.loxon.javachallenge.secondturn.enchancedgamedata.PlanetWithState;
import com.szoftverhurkak.loxon.javachallenge.secondturn.gamedata.MovingArmy;
import com.szoftverhurkak.loxon.javachallenge.secondturn.gamedata.StationedArmy;

import javax.swing.*;
import java.awt.*;
import java.util.*;
import java.util.List;

public class UniverseDrawer {

    private final JFrame universe;
    private final UniversePanel universePanel;
    private List<PlanetWithState> planets;

    public UniverseDrawer() {
        universe = new JFrame();
        universe.setVisible(true);
        universePanel = new UniversePanel();
    }


    public void drawPlanets(List<PlanetWithState> planets) {
        this.planets = planets;
        universe.setContentPane(universePanel);
        universe.repaint();
        universe.pack();
    }

    private class UniversePanel extends JPanel {

        private Map<String, Color> playerColorMap = new HashMap<>();
        private LinkedList<Color> availableColors = new LinkedList<>(Arrays.asList(
                new Color(1f, 0, 0, 1),
                new Color(0, 0, 1f, 1),
                new Color(0, 1f, 0, 1),
                new Color(1f, 1, 0, 1),
                new Color(0, 1f, 1, 1),
                new Color(1f, 0, 1, 1)));

        public UniversePanel() {
            setPreferredSize(new Dimension(1300, 1000));
        }

        @Override
        public void paint(Graphics graphics) {
            super.paint(graphics);
            planets.forEach(planet -> {
                Color ownerColor = getColorForPlayer(planet.getPlanetState().getOwner());
                drawArmies(graphics, planet);
                drawPlanet(graphics, planet, ownerColor);
                graphics.setColor(Color.BLACK);
                drawTexts(graphics, planet);
            });
        }

        public Color getColorForPlayer(String id) {
            Color color = playerColorMap.get(id);
            if (color == null) {
                color = availableColors.removeFirst();
                playerColorMap.put(id, color);
            }
            return color;
        }

        private void drawArmies(Graphics graphics, PlanetWithState planet) {
            drawMovingArmies(graphics, planet.getPlanetState().getMovingArmies());
            graphics.setColor(Color.BLACK);
            drawStationedArmies(graphics, planet.getPlanetState().getStationedArmies(), planet.getPlanet().getX(), planet.getPlanet().getY());
        }

        private void drawPlanet(Graphics graphics, PlanetWithState p, Color ownerColor) {
            int planetDrawSize = getPlanetDrawSize(p);
            double ownershipRatio = p.getPlanetState().getOwnershipRatio();
            Color faintedOwnerColor = new Color(ownerColor.getRed(), ownerColor.getGreen(), ownerColor.getBlue(), (int) (255 * ownershipRatio));
            graphics.setColor(faintedOwnerColor);
            graphics.fillOval(p.getPlanet().getX(), p.getPlanet().getY(),
                    planetDrawSize, planetDrawSize);
        }

        private void drawMovingArmies(Graphics graphics, List<MovingArmy> armies) {
            armies.forEach(army -> {
                Color color = getColorForPlayer(army.getOwner());
                graphics.setColor(color);
                graphics.fillOval((int) army.getX(), (int) army.getY(), army.getSize(), army.getSize());
            });
        }

        private void drawStationedArmies(Graphics graphics, List<StationedArmy> armies, int x, int y) {
            armies.forEach(army -> graphics.fillOval(x, y, army.getSize(), army.getSize()));
        }

        private int getPlanetDrawSize(PlanetWithState p) {
            return p.getPlanet().getRadius();
        }

        private void drawTexts(Graphics graphics, PlanetWithState planet) {
            graphics.drawString("PlanetID: " + planet.getPlanet().getPlanetID(), planet.getPlanet().getX(), planet.getPlanet().getY() - 40);
            graphics.drawString("PlanetSize " + planet.getPlanet().getRadius(), planet.getPlanet().getX(), planet.getPlanet().getY() - 20);
            graphics.drawString("Owner: " + planet.getPlanetState().getOwner(), planet.getPlanet().getX(), planet.getPlanet().getY() + 40);
            graphics.drawString("Ownership ratio: " + planet.getPlanetState().getOwnershipRatio(), planet.getPlanet().getX(), planet.getPlanet().getY() + 60);
        }
    }
}
