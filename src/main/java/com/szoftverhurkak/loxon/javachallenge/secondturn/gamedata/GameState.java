package com.szoftverhurkak.loxon.javachallenge.secondturn.gamedata;

import java.util.List;

public class GameState {
    private List<PlanetState> planetStates;
    private List<Standing> standings;
    private GameStatus gameStatus;
    private int timeElapsed;
    private int remainingPlayers;

    public enum GameStatus {
        PLAYING, ENDED, ABORTED
    }

    public List<PlanetState> getPlanetStates() {
        return planetStates;
    }

    public void setPlanetStates(List<PlanetState> planetStates) {
        this.planetStates = planetStates;
    }

    public List<Standing> getStandings() {
        return standings;
    }

    public void setStandings(List<Standing> standings) {
        this.standings = standings;
    }

    public GameStatus getGameStatus() {
        return gameStatus;
    }

    public void setGameStatus(GameStatus gameStatus) {
        this.gameStatus = gameStatus;
    }

    public int getTimeElapsed() {
        return timeElapsed;
    }

    public void setTimeElapsed(int timeElapsed) {
        this.timeElapsed = timeElapsed;
    }

    public int getRemainingPlayers() {
        return remainingPlayers;
    }

    public void setRemainingPlayers(int remainingPlayers) {
        this.remainingPlayers = remainingPlayers;
    }
}
