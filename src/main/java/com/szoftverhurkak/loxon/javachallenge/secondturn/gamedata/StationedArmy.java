package com.szoftverhurkak.loxon.javachallenge.secondturn.gamedata;

public class StationedArmy extends Army {
    public StationedArmy() {
    }

    public StationedArmy(String owner, int size, int planetId, double x, double y) {
        super(owner, size, planetId, x, y);
    }

    @Override
    public StationedArmy copyWithNewSize(int size) {
        return new StationedArmy(owner, size, planetId, x, y);
    }
}
