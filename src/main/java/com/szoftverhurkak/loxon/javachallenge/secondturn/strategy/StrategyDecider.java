package com.szoftverhurkak.loxon.javachallenge.secondturn.strategy;

import com.szoftverhurkak.loxon.javachallenge.secondturn.enchancedgamedata.PlanetWithState;
import com.szoftverhurkak.loxon.javachallenge.secondturn.gamedata.GameDescription;
import com.szoftverhurkak.loxon.javachallenge.secondturn.gamedata.GameState;

import java.util.List;
import java.util.Map;

public interface StrategyDecider {

    Strategy decide(GameDescription gameDescription, List<GameState> gameStateHistory, Map<Integer, PlanetWithState> planets);
}
