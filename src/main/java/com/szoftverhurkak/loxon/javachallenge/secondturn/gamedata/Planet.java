package com.szoftverhurkak.loxon.javachallenge.secondturn.gamedata;

public class Planet implements Comparable<Planet> {
    private int planetID = 0;
    private int x = 0;
    private int y = 0;
    private int radius = 0;

    public Planet() {
    }

    public Planet(int planetID, int x, int y, int radius) {
        this.planetID = planetID;
        this.x = x;
        this.y = y;
        this.radius = radius;
    }

    public int getPlanetID() {
        return planetID;
    }

    public void setPlanetID(int planetID) {
        this.planetID = planetID;
    }

    public int getX() {
        return x;
    }

    public void setX(int x) {
        this.x = x;
    }

    public int getY() {
        return y;
    }

    public void setY(int y) {
        this.y = y;
    }

    public int getRadius() {
        return radius;
    }

    public void setRadius(int radius) {
        this.radius = radius;
    }

    @Override
    public int compareTo(Planet o) {
        return radius - o.radius;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Planet planet = (Planet) o;

        return planetID == planet.planetID;
    }

    @Override
    public int hashCode() {
        return planetID;
    }
}
