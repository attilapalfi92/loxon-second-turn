package com.szoftverhurkak.loxon.javachallenge.secondturn.strategy;

import com.szoftverhurkak.loxon.javachallenge.secondturn.enchancedgamedata.PlanetWithState;
import com.szoftverhurkak.loxon.javachallenge.secondturn.gamedata.Army;
import com.szoftverhurkak.loxon.javachallenge.secondturn.gamedata.Command;
import com.szoftverhurkak.loxon.javachallenge.secondturn.gamedata.StationedArmy;
import com.szoftverhurkak.loxon.javachallenge.secondturn.util.IntelligenceUtils;
import com.szoftverhurkak.loxon.javachallenge.secondturn.util.Pair;
import com.szoftverhurkak.loxon.javachallenge.secondturn.util.SortedList;

import java.util.Optional;

import static com.szoftverhurkak.loxon.javachallenge.secondturn.util.IntelligenceUtils.*;
import static java.lang.Math.pow;

public class StatCalculation implements Comparable<StatCalculation> {

    private final double bs = getGameDescription().getBattleSpeed();
    private final double be = getGameDescription().getBattleExponent();
    private final double cs = getGameDescription().getCaptureSpeed();
    private final double pe = getGameDescription().getPlanetExponent();
    private final double ucs = getGameDescription().getUnitCreateSpeed();
    private final double pr;

    private final Pair<Integer, ? extends Army> armyWithSize;
    private final Pair<Integer, ? extends Army> army2;
    private final PlanetWithState planet;
    private final SortedList<Pair<Double, Army>> allArrivingArmies;

    private double totalTimeSpent = 0;

    private int alliedArmies;
    private int enemyArmies;
    private double timeToNextArrival;
    private double ownershipRatio;
    private Owner owner;

    private final Owner startingOwner;
    private final boolean isWinnable;
    private final boolean newPlanetOccupied;

    private final double travelTime;

    public StatCalculation(PlanetWithState planet) {
        this.planet = planet;
        travelTime = 0;
        armyWithSize = null;
        army2 = null;

        pr = planet.getPlanet().getRadius();
        Optional<StationedArmy> stationaryAlliedArmy = getStationaryAlliedArmy(planet);
        Optional<StationedArmy> stationaryEnemyArmy = getStationaryEnemyArmy(planet);
        allArrivingArmies = getAllArrivingArmies(planet);

        alliedArmies = stationaryAlliedArmy.map(Army::getSize).orElse(0);
        enemyArmies = stationaryEnemyArmy.map(Army::getSize).orElse(0);
        ownershipRatio = planet.getPlanetState().getOwnershipRatio();
        owner = IntelligenceUtils.isOurPlanet(planet) ? Owner.ALLY : Owner.ENEMY;
        startingOwner = owner;

        calculateStats();
        isWinnable = owner == Owner.ALLY;
        newPlanetOccupied = startingOwner == Owner.ENEMY && owner == Owner.ALLY && isWinnable;
    }

    public StatCalculation(Pair<Integer, ? extends Army> armyWithSize, PlanetWithState planet) {
        this.armyWithSize = armyWithSize;
        army2 = null;
        this.planet = planet;
        double distance = distance(armyWithSize.getRight(), planet);
        travelTime = distance / getGameDescription().getMovementSpeed();

        pr = planet.getPlanet().getRadius();
        Optional<StationedArmy> stationaryAlliedArmy = getStationaryAlliedArmy(planet);
        Optional<StationedArmy> stationaryEnemyArmy = getStationaryEnemyArmy(planet);
        allArrivingArmies = getAllArrivingArmies(planet);

        allArrivingArmies.addInOrder(new Pair<>(travelTime, armyWithSize.getRight().copyWithNewSize(armyWithSize.getLeft())));

        alliedArmies = stationaryAlliedArmy.map(Army::getSize).orElse(0);
        enemyArmies = stationaryEnemyArmy.map(Army::getSize).orElse(0);
        ownershipRatio = planet.getPlanetState().getOwnershipRatio();
        owner = IntelligenceUtils.isOurPlanet(planet) ? Owner.ALLY : Owner.ENEMY;
        startingOwner = owner;

        calculateStats();
        isWinnable = owner == Owner.ALLY;
        newPlanetOccupied = startingOwner == Owner.ENEMY && owner == Owner.ALLY && isWinnable;
    }

    public StatCalculation(PlanetWithState planet, Pair<Integer, ? extends Army> army1, Pair<Integer, ? extends Army> army2) {
        this.armyWithSize = army1;
        this.army2 = army2;
        this.planet = planet;
        double distance = distance(army1.getRight(), planet);
        travelTime = distance / getGameDescription().getMovementSpeed();

        pr = planet.getPlanet().getRadius();
        Optional<StationedArmy> stationaryAlliedArmy = getStationaryAlliedArmy(planet);
        Optional<StationedArmy> stationaryEnemyArmy = getStationaryEnemyArmy(planet);
        allArrivingArmies = getAllArrivingArmies(planet);

        double army2TravelTime = distance(army2.getRight(), planet) / getGameDescription().getMovementSpeed();
        allArrivingArmies.addInOrder(new Pair<>(travelTime, army1.getRight().copyWithNewSize(army1.getLeft())));
        allArrivingArmies.addInOrder(new Pair<>(
                army2TravelTime + (((double) getGameDescription().getCommandSchedule()) / 1000),
                army2.getRight().copyWithNewSize(army2.getLeft())
        ));

        alliedArmies = stationaryAlliedArmy.map(Army::getSize).orElse(0);
        enemyArmies = stationaryEnemyArmy.map(Army::getSize).orElse(0);
        ownershipRatio = planet.getPlanetState().getOwnershipRatio();
        owner = IntelligenceUtils.isOurPlanet(planet) ? Owner.ALLY : Owner.ENEMY;
        startingOwner = owner;

        calculateStats();
        isWinnable = owner == Owner.ALLY;
        newPlanetOccupied = startingOwner == Owner.ENEMY && owner == Owner.ALLY && isWinnable;
    }

    public Optional<Command> getCommand() {
        if (armyWithSize != null) {
            return Optional.of(new Command(armyWithSize.getRight().getPlanetID(),
                    planet.getPlanet().getPlanetID(),
                    armyWithSize.getLeft()));
        }
        return Optional.empty();
    }

    public Optional<Command> getCommandForArmy2() {
        if (armyWithSize != null) {
            return Optional.of(new Command(army2.getRight().getPlanetID(),
                    planet.getPlanet().getPlanetID(),
                    army2.getLeft()));
        }
        return Optional.empty();
    }

    private void calculateStats() {

        while (!allArrivingArmies.isEmpty()) {
            Pair<Double, Army> firstArriving = allArrivingArmies.removeLast();

            if (firstArriving != null) {
                timeToNextArrival = firstArriving.getLeft() - totalTimeSpent;

                if (alliedArmies > 0 && enemyArmies > 0) {
                    calculateBattleTime();

                } else if (alliedArmies > 0 || enemyArmies > 0) {
                    calculateOccupationOrProductionForTime(timeToNextArrival);

                } else {
                    // only production can happen in this case
                    calculateProductionForTime(timeToNextArrival);
                }

                if (isOurArmy(firstArriving.getRight())) {
                    alliedArmies += firstArriving.getRight().getSize();
                } else {
                    enemyArmies += firstArriving.getRight().getSize();
                }
            }
        }

        timeToNextArrival = Double.MAX_VALUE;

        if (alliedArmies > 0 && enemyArmies > 0) {
            calculateBattleTime();
        } else if (alliedArmies > 0) {
            calculateOccupationUntil100Percent();
        } else {
            calculateOccupationUntil100Percent();
        }
    }

    private void calculateBattleTime() {

        final double timeToFinishBattle;
        final double battleTime;

        if (alliedArmies > enemyArmies) {
            timeToFinishBattle = enemyArmies / (bs * pow(alliedArmies, be) / (alliedArmies + enemyArmies));
            battleTime = timeToFinishBattle <= timeToNextArrival ? timeToFinishBattle : timeToNextArrival;

        } else {
            timeToFinishBattle = alliedArmies / (bs * pow(enemyArmies, be) / (alliedArmies + enemyArmies));
            battleTime = timeToFinishBattle <= timeToNextArrival ? timeToFinishBattle : timeToNextArrival;
        }

        int alliedLost = (int) Math.round(battleTime * bs * pow(enemyArmies, be) / (alliedArmies + enemyArmies));
        int enemyLost = (int) Math.round(battleTime * bs * pow(alliedArmies, be) / (alliedArmies + enemyArmies));
        alliedArmies -= alliedLost;
        enemyArmies -= enemyLost;

        totalTimeSpent += battleTime;

        if (timeToNextArrival == Double.MAX_VALUE) {
            // no new arriving armies so the winner army can occupy
            calculateOccupationUntil100Percent();
        } else {
            if (battleTime < timeToNextArrival) {
                // until next army arrives, the winner can occupy the planet or produce armies
                // for the time left
                calculateOccupationOrProductionForTime(timeToNextArrival - timeToFinishBattle);
            }
            // else: battle lasted until next army arrives so new battle
            // calculations needed with new army sizes
        }
    }

    // (sereg létszáma) * {captureSpeed} / (bolygó sugara){planetExponent}
    // this method assumes there is strictly 1 army on the planet. enemy or allied.
    // also assumes that timeToNextArrival is a valid time

    private void calculateOccupationOrProductionForTime(double timeToNextArrival) {

        if (owner == Owner.ENEMY) {

            if (alliedArmies > 0) {
                double ratioToGetOwnership = ownershipRatio;
                double timeToGetOwnership = ratioToGetOwnership * pow(pr, pe) / (alliedArmies * cs);

                if (timeToGetOwnership <= timeToNextArrival) {
                    owner = Owner.ALLY;

                    double timeLeftToOccupy = timeToNextArrival - timeToGetOwnership;
                    double timeToGain100Percent = 1 * pow(pr, pe) / (alliedArmies * cs);

                    if (timeToGain100Percent <= timeLeftToOccupy) {
                        ownershipRatio = 1;
                        totalTimeSpent += (timeToGetOwnership + timeToGain100Percent);
                        calculateProductionForTime(timeLeftToOccupy - timeToGain100Percent);
                    } else {
                        totalTimeSpent += timeToNextArrival;
                        ownershipRatio = (alliedArmies * cs * timeLeftToOccupy) / pow(pr, pe);
                    }

                } else {
                    double ownershipReduction = (alliedArmies * cs * timeToNextArrival) / pow(pr, pe);
                    totalTimeSpent += timeToNextArrival;
                    ownershipRatio -= ownershipReduction;
                }
            }

            if (enemyArmies > 0) {
                if (ownershipRatio == 1) {

                    calculateProductionForTime(timeToNextArrival);
                } else {

                    double ratioToGain100Percent = 1 - ownershipRatio;
                    double timeToGain100Percent = ratioToGain100Percent * pow(pr, pe) / (enemyArmies * cs);

                    if (timeToGain100Percent <= timeToNextArrival) {
                        ownershipRatio = 1;
                        totalTimeSpent += timeToGain100Percent;
                        calculateProductionForTime(timeToNextArrival - timeToGain100Percent);
                    } else {
                        totalTimeSpent += timeToNextArrival;
                        ownershipRatio = (enemyArmies * cs * timeToNextArrival) / pow(pr, pe);
                    }
                }
            }
        }

        if (owner == Owner.ALLY) {

            if (enemyArmies > 0) {

                double ratioToGetOwnership = ownershipRatio;
                double timeToGetOwnership = ratioToGetOwnership * pow(pr, pe) / (enemyArmies * cs);

                if (timeToGetOwnership <= timeToNextArrival) {
                    owner = Owner.ENEMY;

                    double timeLeftToOccupy = timeToNextArrival - timeToGetOwnership;
                    double timeToGain100Percent = 1 * pow(pr, pe) / (enemyArmies * cs);

                    if (timeToGain100Percent <= timeLeftToOccupy) {
                        ownershipRatio = 1;
                        totalTimeSpent += (timeToGetOwnership + timeToGain100Percent);
                        calculateProductionForTime(timeLeftToOccupy - timeToGain100Percent);
                    } else {
                        totalTimeSpent += timeToNextArrival;
                        ownershipRatio = (enemyArmies * cs * timeLeftToOccupy) / pow(pr, pe);
                    }

                } else {
                    double ownershipReduction = (enemyArmies * cs * timeToNextArrival) / pow(pr, pe);
                    totalTimeSpent += timeToNextArrival;
                    ownershipRatio -= ownershipReduction;
                }
            }

            if (alliedArmies > 0) {
                if (ownershipRatio == 1) {

                    calculateProductionForTime(timeToNextArrival);
                } else {

                    double ratioToGain100Percent = 1 - ownershipRatio;
                    double timeToGain100Percent = ratioToGain100Percent * pow(pr, pe) / (alliedArmies * cs);

                    if (timeToGain100Percent <= timeToNextArrival) {
                        ownershipRatio = 1;
                        totalTimeSpent += timeToGain100Percent;
                        calculateProductionForTime(timeToNextArrival - timeToGain100Percent);
                    } else {
                        totalTimeSpent += timeToNextArrival;
                        ownershipRatio = (alliedArmies * cs * timeToNextArrival) / pow(pr, pe);
                    }
                }
            }
        }
    }

    private void calculateOccupationUntil100Percent() {
        final double timeToGetOwnership;
        final double timeToGain100Percent;
        if (owner == Owner.ALLY) {
            if (alliedArmies > 0) {
                timeToGetOwnership = 0;
                double ratioToGain100Percent = 1 - ownershipRatio;
                timeToGain100Percent = ratioToGain100Percent * pow(pr, pe) / (alliedArmies * cs);
            } else { // enemyArmies > 0
                double ratioToGetOwnership = ownershipRatio;
                timeToGetOwnership = ratioToGetOwnership * pow(pr, pe) / (enemyArmies * cs);
                timeToGain100Percent = 1 * pow(pr, pe) / (enemyArmies * cs);
            }
        } else { // owner == Owner.ENEMY
            if (enemyArmies > 0) {
                timeToGetOwnership = 0;
                double ratioToGain100Percent = 1 - ownershipRatio;
                timeToGain100Percent = ratioToGain100Percent * pow(pr, pe) / (enemyArmies * cs);
            } else { // alliedArmies > 0
                double ratioToGetOwnership = ownershipRatio;
                timeToGetOwnership = ratioToGetOwnership * pow(pr, pe) / (alliedArmies * cs);
                timeToGain100Percent = 1 * pow(pr, pe) / (alliedArmies * cs);
            }
        }

        if (alliedArmies > 0) {
            owner = Owner.ALLY;
        } else if (enemyArmies > 0) {
            owner = Owner.ENEMY;
        }

        double timeSpentOccupying = timeToGetOwnership + timeToGain100Percent;
        totalTimeSpent += timeSpentOccupying;
        ownershipRatio = 1;
    }

    // (bolygó sugara){planetExponent} * {unitCreateSpeed}
    private void calculateProductionForTime(double timeToProduceUnits) {
        totalTimeSpent += timeToProduceUnits;
        if (ownershipRatio == 1) {
            int producedUnits = (int) Math.round(pow(pr, pe) * ucs * timeToProduceUnits);
            if (owner == Owner.ALLY) {
                alliedArmies += producedUnits;
            } else {
                enemyArmies += producedUnits;
            }
        }
    }

    public double getTotalTimeSpent() {
        return totalTimeSpent;
    }

    public int getAlliedArmiesLeft() {
        return alliedArmies;
    }

    public int getEnemyArmiesLeft() {
        return enemyArmies;
    }

    public boolean isWinnable() {
        return isWinnable;
    }

    public double getTravelTime() {
        return travelTime;
    }

    public boolean isNewPlanetOccupied() {
        return newPlanetOccupied;
    }

    public Pair<Integer, ? extends Army> getArmyWithSize() {
        return armyWithSize;
    }

    public Pair<Integer, ? extends Army> getArmyWithSize2() {
        return army2;
    }

    public PlanetWithState getPlanet() {
        return planet;
    }

    @Override
    public int compareTo(StatCalculation o) {
        return Double.compare(totalTimeSpent, o.totalTimeSpent);
    }

    private enum Owner {
        ENEMY, ALLY
    }
}
