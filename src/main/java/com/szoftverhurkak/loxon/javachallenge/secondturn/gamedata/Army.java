package com.szoftverhurkak.loxon.javachallenge.secondturn.gamedata;

import com.szoftverhurkak.loxon.javachallenge.secondturn.util.Pair;

public abstract class Army implements Comparable<Army> {
    private int id = (int) (Math.random() * Integer.MAX_VALUE);
    protected String owner = "";
    protected int size = 0;
    protected int planetId = 0;
    protected double x = 0;
    protected double y = 0;

    public Army() {
    }

    public Army(String owner, int size, int planetId, double x, double y) {
        this.owner = owner;
        this.size = size;
        this.planetId = planetId;
        this.x = x;
        this.y = y;
    }

    abstract public Army copyWithNewSize(int size);

    public Pair<Integer, Army> getArmyWithSizeWithActualSize() {
        return new Pair<>(size, this);
    }

    @Override
    public int compareTo(Army o) {
        return Integer.compare(o.getSize(), size);
    }

    public String getOwner() {
        return owner;
    }

    public void setOwner(String owner) {
        this.owner = owner;
    }

    public int getSize() {
        return size;
    }

    public void setSize(int size) {
        this.size = size;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Army army = (Army) o;

        return id == army.id;
    }

    @Override
    public int hashCode() {
        return id;
    }

    public int getPlanetID() {
        return planetId;
    }

    public void setPlanetId(int planetId) {
        this.planetId = planetId;
    }

    public double getX() {
        return x;
    }

    public void setX(int x) {
        this.x = x;
    }

    public double getY() {
        return y;
    }

    public void setY(int y) {
        this.y = y;
    }
}
