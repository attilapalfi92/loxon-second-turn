package com.szoftverhurkak.loxon.javachallenge.secondturn;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.szoftverhurkak.loxon.javachallenge.secondturn.gamedata.Command;
import com.szoftverhurkak.loxon.javachallenge.secondturn.gamedata.GameDescription;
import com.szoftverhurkak.loxon.javachallenge.secondturn.gamedata.GameState;

import javax.websocket.Endpoint;
import javax.websocket.EndpointConfig;
import javax.websocket.MessageHandler;
import javax.websocket.Session;
import java.io.IOException;

public class ClientEndpoint extends Endpoint implements MessageHandler.Whole<String>, CommandSender {

    private final GameLogic gameLogic;
    private final ObjectMapper objectMapper = new ObjectMapper();
    private Session session;
    private boolean firstMessageReceived = false;

    public ClientEndpoint(GameLogic gameLogic) {
        this.gameLogic = gameLogic;
    }

    @Override
    public void onOpen(Session session, EndpointConfig config) {
        session.addMessageHandler(this);
        this.session = session;
    }

    @Override
    public void onMessage(String message) {
        System.out.println(message);
        try {
            if (!firstMessageReceived) {
                GameDescription gameDescription = objectMapper.readValue(message, GameDescription.class);
                gameLogic.setGameDescription(gameDescription, this);
                firstMessageReceived = true;
            } else {
                gameLogic.updateGameState(objectMapper.readValue(message, GameState.class), this);
            }
        } catch (IOException e) {
            firstMessageReceived = !firstMessageReceived;
            e.printStackTrace();
        }
    }

    @Override
    public void sendCommand(Command command) {
        session.getAsyncRemote().sendText(command.toString());
    }
}
