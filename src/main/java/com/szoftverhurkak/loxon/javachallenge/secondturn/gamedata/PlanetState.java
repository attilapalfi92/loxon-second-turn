package com.szoftverhurkak.loxon.javachallenge.secondturn.gamedata;

import java.util.ArrayList;
import java.util.List;

public class PlanetState {
    private int planetID;
    private String owner;
    private double ownershipRatio;
    private List<MovingArmy> movingArmies = new ArrayList<>();
    private List<StationedArmy> stationedArmies = new ArrayList<>();

    public int getPlanetID() {
        return planetID;
    }

    public void setPlanetID(int planetID) {
        this.planetID = planetID;
    }

    public String getOwner() {
        return owner;
    }

    public void setOwner(String owner) {
        this.owner = owner;
    }

    public double getOwnershipRatio() {
        return ownershipRatio;
    }

    public void setOwnershipRatio(double ownershipRatio) {
        this.ownershipRatio = ownershipRatio;
    }

    public List<MovingArmy> getMovingArmies() {
        return movingArmies;
    }

    public void setMovingArmies(List<MovingArmy> movingArmies) {
        this.movingArmies = movingArmies;
    }

    public List<StationedArmy> getStationedArmies() {
        return stationedArmies;
    }

    public void setStationedArmies(List<StationedArmy> stationedArmies) {
        this.stationedArmies = stationedArmies;
    }
}
