package com.szoftverhurkak.loxon.javachallenge.secondturn.strategy;

import com.szoftverhurkak.loxon.javachallenge.secondturn.enchancedgamedata.PlanetWithState;
import com.szoftverhurkak.loxon.javachallenge.secondturn.gamedata.GameDescription;
import com.szoftverhurkak.loxon.javachallenge.secondturn.gamedata.GameState;

import java.util.List;
import java.util.Map;

public class DummyStrategyDecider implements StrategyDecider {

    private final AttilaStrategy attilaStrategy = new AttilaStrategy();

    @Override
    public Strategy decide(GameDescription gameDescription, List<GameState> gameStateHistory, Map<Integer, PlanetWithState> planets) {
        return attilaStrategy;
    }
}
